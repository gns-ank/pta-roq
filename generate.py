#! /usr/bin/env python2.7

#{{{
# Import some things from Python 3
from __future__ import print_function, division, unicode_literals
# Useful for stamping and calling the compiled binaries
from time import gmtime, strftime
from subprocess import call
import sys
# Plotting stuff
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import csv2rec
#}}}
#{{{ Parameters
# Import some os stuff
#stamp = strftime("%F", gmtime())
stamp_dat = "2013-08-01"
stamp_roq = "2013-08-01"
pulsarNumber="36"
t_final="5"
dt_min="2"
dt_max="2"

# ROQ Error
error = "1e-7"

rb_gen = "bin/rb_generator"
rb_gen_rc = "rc/rb-IPTA-like"
rb_gen_param_rc = rb_gen_rc + "-params"
roq_gen = "bin/roq_generator"
roq_gen_rc = "rc/roq-IPTA-like"
data_gen = "bin/data_generator"
data_gen_rc = "rc/data-IPTA-like"
data_gen_source_rc = data_gen_rc + "-sources-1"
error_gen = "bin/error_test"
#}}}
#{{{ Functions
def gen_data(N,M):
    """
    Generate the data for different number of pulsars. This basically creates a noisy signal and
    exports it to a file.

    Parameters:
        N: The increment in the number of pulsars
        M: Number of data sets
    """
    # Make the binaries, just in case
    call(["make"])

    for i in xrange(1,M+1):
        pulsarNumber = str(N*i)
        stamp_tmp = stamp_dat + "-" + str(i)
        call([data_gen, data_gen_rc, stamp_tmp, data_gen_source_rc, pulsarNumber, t_final, dt_min, dt_max])

def gen_rb(N,M):
    """
    Generate the reduced basis set. This will find reduced basis in a given parameter range.

    Parameters:
        N: The number of patches in the parameter space to investigate
        M: Number of data sets to investigate
    """
    # Make the binaries, just in case
    call(["make"])

    for j in xrange(1,N+1):
        rb_gen_param_rc_tmp = rb_gen_param_rc + "-" + str(j)
        for i in xrange(1,M+1):
            stamp_tmp_in = stamp_dat + "-" + str(i)
            stamp_tmp = stamp_roq + "-" + str(i) + "-param-" + str(j)
            call([rb_gen, rb_gen_rc, rb_gen_param_rc_tmp, stamp_tmp, stamp_tmp_in, error ])

def gen_roq(N, M):
    """
    Generate the quadrature rules and the modified covariance matrix. This will use the reduced
    basis things to do that.

    Parameters:
        N: The number of patches in the parameter space to investigate
        M: Number of data sets to investigate
    """
    # Make the binaries, just in case
    call(["make"])

    for j in xrange(1,N+1):
        roq_gen_param_rc_tmp = rb_gen_param_rc + "-" + str(j)
        for i in xrange(1,M+1):
            stamp_tmp_in = stamp_dat + "-" + str(i)
            stamp_tmp = stamp_roq + "-" + str(i) + "-param-" + str(j)
            call([roq_gen, roq_gen_rc, data_gen_source_rc, stamp_tmp, stamp_tmp_in, error ])


def gen_innerProdErrors(N,M) :
    """
    Generate the estimates of inner product errors depending on the number of reduced basis we take.

    Parameters:
        N: The number of patches in the parameter space to investigate
        M: Number of data sets to investigate
    """
    call(["make"])

    files_in_source = [ "sources-2", "sources-2" ]
    files_in_dat = [ "pulsars", "schedule" ]
    files_in_roq = [ "rbparams", "eim-points" ]
    files_out = [ "error_rh", "error_hh" ]

    # Generate the filenames
    stamp = stamp_dat
    for i in xrange(2):
        files_in_source[i] = data_gen_rc + "-" + files_in_source[i];

    for j in xrange(1,N+1):
        for i in xrange(1,M+1):
            stamp_1 = stamp + "-" + str(i) + "-"
            stamp_2 = stamp_1 + "param-" + str(j) + "-"
            fio_tmp = [
                    "data/dat-" + stamp_1 + files_in_dat[0] + ".csv",
                    "data/dat-" + stamp_1 + files_in_dat[1] + ".csv",
                    "data/roq-" + stamp_2 + files_in_roq[0] + ".csv",
                    "data/roq-" + stamp_2 + files_in_roq[1] + ".csv",
                    "data/err-" + stamp_2 + files_out[0] + ".csv",
                    "data/err-" + stamp_2 + files_out[1] + ".csv",
                    ]
            call([error_gen] + files_in_source + fio_tmp)

def plot_innerProdErrors (N,M,NP):
    """
    Generate the estimates of inner product errors depending on the number of reduced basis we take.

    Parameters:
        N: The number of patches in the parameter space to investigate
        M: Number of data sets to investigate
        NP: The increment in the number of pulsars
    """
    files = [ "error_rh", "error_hh" ]

    # Now plot everything and calculate at the same time
    fig = plt.figure();
    ax = fig.add_subplot(111)
    ax.set_xlabel(r'# of basis in the RB set')
    ax.set_ylabel(r'The percentage error of inner products')
    # A trick I found on http://stackoverflow.com/questions/7187504
    ax.set_autoscale_on(True)
    ax.autoscale_view(True,True,True)
    ax.set_yscale('log')
    call(["mkdir", "-p", "plots"])

    data1, = ax.plot([],[])
    data2, = ax.plot([],[])
    ax.legend([data1, data2], ["rh", "hh"])

    for j in xrange(1,N+1):
        for i in xrange(1,M+1):
            stamp_1 = stamp_dat + "-" + str(i) + "-" + "param-" + str(j) + "-"
            fio_tmp = [
                    "data/err-" + stamp_1 + files[0] + ".csv",
                    "data/err-" + stamp_1 + files[1] + ".csv",
                    ]

            pulsarNumber = str(NP*i)
            ax.set_title(r'Error in inner products in likelihoods for ' + pulsarNumber + ' pulsars')

            try:
                with open(fio_tmp[0]) as infile1, open(fio_tmp[1]) as infile2:
                    print("Plotting " + fio_tmp[0] + " and " + fio_tmp[1])
                    error1 = np.loadtxt(infile1, delimiter=",")
                    error2 = np.loadtxt(infile2, delimiter=",")

                    data1.set_data(np.arange(0,error1.shape[0]),error1)
                    data2.set_data(np.arange(0,error2.shape[0]),error2)
                    ax.relim()
                    ax.autoscale_view(True,True,True)

                    plt.show()

                    plt.savefig("plots/err-" + stamp_1 + "error-rh-hh.pdf")
            except IOError:
                print(fio_tmp[0] + " or " + fio_tmp[1] + " not present")
                break

def plot_RbErrors(N,M,NP):
    """
    Generate the estimates of inner product errors depending on the number of reduced basis we take.

    Parameters:
        N: The number of patches in the parameter space to investigate
        M: Number of data sets to investigate
        NP: The increment in the number of pulsars
    """
    fig = plt.figure();
    ax = fig.add_subplot(111)
    ax.set_xlabel(r'# of basis in the RB set')
    ax.set_ylabel(r'The percentage error w.r.t. the largest norm in the training set')
    # A trick I found on http://stackoverflow.com/questions/7187504
    ax.set_autoscale_on(True)
    ax.autoscale_view(True,True,True)
    ax.set_yscale('log')
    call(["mkdir", "-p", "plots"])

    data, = ax.plot([],[])
    # Read the error files
    for j in xrange(1,N+1):
        for i in xrange(1,M+1):
            pulsarNumber = str(NP*i)
            ax.set_title(r'Error evolution during the greedy algorithm for ' + pulsarNumber + ' pulsars')
            stamp_tmp = stamp_roq + "-" + str(i) + "-param-" + str(j)
            errorname = "data/roq-" + stamp_tmp + "-rb.csv"

            print("Plotting " + errorname)
            try:
                with open(errorname) as infile:
                    error = np.loadtxt(open(errorname),delimiter=",")
            except IOError:
                print(errorname + " not present")
                break

            data.set_data(np.arange(0,error.shape[0]),error)
            ax.relim()
            ax.autoscale_view(True,True,True)

            plt.savefig("plots/roq-" + stamp_tmp + "-rb.pdf")
#}}}

# Execute the stuff

gen_data(18,6)
gen_rb(1,1)
gen_roq (1,1)
gen_innerProdErrors (1,1)
plot_innerProdErrors (1,1,6)
#plot_RbErrors (1,1,6)

# vim: fdm=marker
