\documentclass[
    fontsize=11pt,
    DIV=11,
    BCOR=5mm,
]{scrreprt}

\usepackage{fontspec}

\usepackage{amsmath, amssymb, bm}
\usepackage{algpseudocode,algorithm,algorithmicx}

\usepackage[
    backend=biber,
    style=numeric
]{biblatex}

\usepackage{hyperref}

\addbibresource{../../papers/pta-references.bib}

\newcommand{\bigo}[1]{\ensuremath{\mathcal{O}\left( #1 \right)}}
\renewcommand{\vec}[1]{\ensuremath{\boldsymbol{#1}}}
\newcommand{\arr}[1]{\ensuremath{\mathrm{#1}}}
\newcommand{\loglike}{\ensuremath{\ln\mathcal{L}}}
\newcommand{\argmax}{\ensuremath{\mathrm{argmax}}}
\newcommand{\cpp}{\texttt{C++}}
\newcommand{\interp}{\ensuremath{\mathcal{I}}}

% bra ket notation macros
\newcommand{\bra}[1]{\ensuremath{\left\langle#1\right|}}
\newcommand{\ket}[1]{\ensuremath{\left|#1\right\rangle}}
\newcommand{\braket}[2]{\ensuremath{\left\langle #1 \middle| #2 \right\rangle}}
\newcommand{\matrixel}[3]{\ensuremath{\left\langle #1 \middle| #2 \middle| #3 \right\rangle}}
\newcommand{\norm}[1]{\ensuremath{\left|\left| #1 \right|\right|}}

\author{Ignas Anikevičius}
\title{Reduced Order Quadrature techniques in the context of Pulsar Timing Array
    data analysis}

\begin{document}
    \maketitle
    \tableofcontents

    \chapter{Introduction}

    \chapter{Background Theory}

    In this chapter I will go through the context of this research as well as
    derivations and definitions of some formulae and mathematical notation used
    through this report.
    %

    \section{Pulsar Timing and Gravitational Wave Search}

    Pulsars are highly predictable cosmic clocks, some of which can rival even
    atomic clocks as shown by <citation needed>.
    %
    At the moment there is an ongoing effort to establish collaborations between
    many radio telescopes in order to make observations of pulsars across the
    whole sky, which is the goal of the International Pulsar Timing Array (IPTA)
    \autocite{IPTA2009}.

    The idea, that pulsars can be used for investigation of gravitational
    waves by investigating correlated fluctuations of time of arrival (TOA)
    measurements is not new <citation needed>.
    %
    %However, only recently data analysis has been started as sufficient amount of
    %data has been gathered <citation needed>.
    %
    The main interest is in seeing whether the residuals of the timing model
    carry any information of gravitational wave source and how to extract it
    quickly.
    %
    The residual in this report will be defined as follows:
    %
    \begin{equation}
        \delta \vec{t} = \vec{t}_{TOA} - \vec{t}_{model}
    \end{equation}

    For this task usually we need quite a lot of data from the fastest and most
    stable millisecond pulsars.
    %
    The residuals are then obtained by fitting the times of arrival (TOAs) to the
    timing model, which removes some degrees of freedom from the actual data.
    %
    In order to cope with this issue the residuals need to be marginalised over
    the timing model parameters, so that the actual data is independent of the
    fitting model.

    The scope of this project is to develop a new way of processing the
    marginalised residuals and, hence, we shall not concern ourselves with the
    marginalisation of the fitting parameters at this stage and we will assume,
    that the residuals are known to be exact within some error.
    %
    This will give more transparency to the derivation and the analysis of the
    method.

    \section{The theoretical model for the signal}

    % TODO I need to find a way to derive the used formula. Look at the Ellis
    % paper and references, because he has done it well, IIRC...
    The theoretical model for this problem comes from the integration of the
    perturbation of the metric along the path of the photon coming from the pulsar
    to the solar system barycentre (SSB).
    %
    Since the travelling particle is a photon, we are concerned only with the time
    components of the perturbation.
    %
    The perturbation comes from either many SMBHBs causing a stochastic
    gravitational background or from a single nearby SMBHB.
    %
    The model for the waveforms emitted by SMBHBs can be found at <citation
    needed> and the actual model for the PTA data which will be used for the PTA
    data generation and analysis was first presented by \textcite{Ellis2012}.

    There will be some modifications to the actual formulae used, but it does not
    influence the final result in any way.

    %TODO finish this bit

    \section{Search of single sources}

    When the data is being processed, usually Markov Chain Monte Carlo (MCMC)
    methods are chosen to maximise the Bayesian likelihood function.
    %
    In this case we use the probability of noise being defined as
    \autocite{Ellis2012}:
    %
    \begin{equation}
        p \left( \vec{n} \right) = \cfrac{1}{\sqrt{\det 2\pi \arr{C}}}
        \exp{\left( - \frac{1}{2} \vec{n}^{T} \arr{C}^{-1} \vec{n} \right) }
    \end{equation}
    %
    If we assume that our residuals consist of a signal and a noise, we can
    rearrange the expression for noise:
    %
    \begin{equation}
        \vec{n} = \delta \vec{t} - \vec{h}
    \end{equation}
    %
    where $\vec{h}$ is the signal template which is being fitted.

    The matrix $\arr{C}$ in the inner product in the exponent is called covariance
    matrix, which contains the information about the correlations between the
    noise series $\vec{n}_i$ for each $i$th pulsar.
    %
    It is symmetric and the form of it can be rationalized as follows:
    %
    \begin{equation}
        C_{ij} = C^{GWB}_{ij} + C^{WN}_{ij} + C^{RN}_{ij} + C^{PLN}_{ij} = \left<
        \vec{n}_i \vec{n}_j \right>
    \end{equation}
    %
    where the angular brackets denote the cross-correlation of the noise time
    series from each pulsar \autocite{Ellis2012}.
    %
    As you can see from the same expression, the matrix has various contributions
    from stochastic processes --- Gravitational Wave Background (GWB), the white
    (WN), red (RN) and power-law noises (PLN) \autocite{vHLML2009}.

    The matrix present in the inner product in the exponent of the multivariate
    Gaussian makes them particularly expensive to evaluate as the operation scales
    as $\bigo{n^{3}}$, where $n$ is the dimensionality of the vectors.
    %
    Also, because of the fact, that the matrix needs to be inverted before
    carrying out evaluations, high computation power is needed to fit several
    noise distributions.

    From now on, we shall use a shorthand notation for such inner products, by
    employing the Dirac's bra-ket notation:
    %
    \begin{equation}
        \braket{\vec{a}}{\vec{b}} = \vec{a}^{T} \arr{C}^{-1} \vec{b} 
    \end{equation}
    %
    Given this notation, we can express the logarithm of the likelihood (\loglike)
    as:
    %
    \begin{equation}
        \loglike = \braket{\delta\vec{t}}{\vec{h}} 
        -\cfrac{1}{2} \braket{\vec{h}}{\vec{h}}
    \end{equation}

    During the MCMC algorithm run, we would try to maximize the log-likelihood by
    varying the signal template and since the dimensions of the vectors used in
    this calculation tend to be large, these are computationally very intensive.
    %
    If we were to use the Multinest <citation needed> algorithm instead of the
    MCMC method, this still would be an issue, although the degeneracies in the
    posterior distribution would be handled better.

    % FIXME finish

    \chapter{Reduced Order Modelling}

    Reduced order modelling is not new and such methods rely on a single
    assumption, that the dimensionality of the problem can be reduced by trying to
    assess the information content.
    %
    Quite recently there was a paper published by \textcite{Canizares2013}
    which investigates such models in the context of LIGO <citation needed>
    detector.
    %
    It relies on three crucial steps before the MCMC method application in the
    maximization of the likelihood:
    %
    \begin{enumerate}
        \item Construction of the reduced basis (RB) set using a region of
              parameter space of interest.
        \item Construction of the interpolation scheme for approximating waveforms
              which are calculated using parameter space points lying close to
              parameter space points used for calculating the RB set.
        \item Construction of the quadrature rules for the inner products used in
              the log-likelihood evaluation.
    \end{enumerate}
    %

    % FIXME finish

    \section{Reduced basis set construction}

    % FIXME finish

    It is important to note, that during this stage we can pre-compute the norm of
    the signal (i.e. \braket{\vec{h}}{\vec{h}}), which is very useful for
    constructing the reduced basis \emph{and} posterior evaluation.
    %
    This means, that during the MCMC cycles we need to compute only one product,
    which we shall speed up by using the Reduced Order Quadrature (ROQ) rule,
    which is described in the next section.
    
    The method used for RB construction relies on the method outlined in the paper
    by \textcite{Canizares2013}, where they use a greedy algorithm, which is
    outline in one of the appendices in the paper.
    %
    However, some improvements can be done when calculating the error of the
    approximation.
    %
    This is because when we are evaluating how well the RB spans the signal
    template space, we calculate the following:
    %
    \begin{equation}
        \sigma^{2}_{RB} = \norm{\vec{h} - \sum_{i} c_{i} \vec{e}_{i}}^{2}
    \end{equation}
    %
    which could be expanded as (note that our covariance matrix used in the inner
    products is symmetric, which means that we have only one type of cross-term
    involved):
    %
    \begin{equation}
        \sigma^{2}_{RB}
        =
        \norm{\vec{h}}^{2}
        - 2 \sum_{i} c_{i} \braket{\vec{h}}{\vec{e}_{i}}
        + \sum_{i,j} c_i c_j \braket{\vec{e}_{i}}{\vec{e}_{j}}
    \end{equation}
    %
    where the inner product between the basis vectors can be identified as the
    Gram matrix and the $c_{i}$ are the projection coefficients on to the reduced
    basis.
    %
    Since the basis on which we project is not necessarily orthogonal (which also
    implies that the Gram matrix might not be identity), we need to use the
    following to calculate $c_{i}$:
    %
    \begin{equation}
        c_i = \sum_{j} G^{-1}_{ij} \braket{\vec{h}}{\vec{e}_j} = \sum_{j}
        G^{-1}_{ij} \widetilde{c}_{j}
    \end{equation}

    By inserting this expression into the full expression for the error, we can
    obtain a simplified expression, which eliminates the cross-term completely:
    %
    \begin{align}
        \sigma^{2}_{RB} &= \norm{\vec{h}}^{2}
        - 2 \sum_{i} c_{i} \widetilde{c}_i + \sum_{i,j} G^{-1}_{jk} c_{i} c_{j}
        \\
        &= \norm{\vec{h}}^{2}
        - 2 \sum_{i,j} G^{-1}_{ij} \widetilde{c}_{i} \widetilde{c}_{j}
        + \sum_{i,j,k,l} G_{ij} G^{-1}_{jk} G^{-1}_{il} \widetilde{c}_{i} \widetilde{c}_{j}
        \\
        &= \norm{\vec{h}}^{2}
        - \sum_{i,j} G^{-1}_{ij} \widetilde{c}_{i} \widetilde{c}_{j}
    \end{align}

    By using these simple linear algebra identities we can construct an algorithm,
    which will build the RB set quicker and will avoid redundant computations.
    %
    This is quite important, as the start-up phase of the Bayesian analysis
    becomes cheaper and in some cases this might make a huge difference in
    deciding whether to use this approach or not.
    %
    The algorithm listed in Algorithm~\ref{algo:rb1} on page~\pageref{algo:rb1}.
    
    \begin{algorithm}
        \caption{A more efficient algorithm for calculating reduced bases}
        \label{algo:rb1}
        \begin{algorithmic}[1]
            \State Input: $\epsilon$, $\{\vec{\lambda}_{i}\}_{i=0}^{M}$

            \State Calculate: $\{\norm{\vec{h} \left( \vec{\lambda}_i \right)}\}_{i=0}^{M}$

            \State $i = 0$
            \State $\sigma_{i} = 1$
            \State $\vec{\Lambda}_{i} = \norm{\vec{h} \left( \vec{\lambda}_i \right)}_{\argmax}$
            \State $RB = \{ \vec{h} \left(\vec{\lambda}_i \right) \}$

            \While{$\sigma_{i} > \epsilon$}
            \State $i = i+1$
            \State $\{c_{j,i-1} 
                = \braket{\vec{h} \left( \vec{\lambda}_j \right)}{\vec{e}_{i-1}} 
            \}_{j=0}^{M}$
            \State $\sigma_{i} = \left( \norm{\vec{h}}_j - \sum_k^{i-1}
                G^{-1}_{kl}c_{j,k} c_{j,l}\right)_{\max}$
            \State $\vec{\lambda}_{i} = \left( \norm{\vec{h}}_j - \sum_k^{i-1}
                G^{-1}_{kl}c_{j,k} c_{j,l}\right)_{\argmax}$
            \State $RB = RB \bigcup \{ \vec{h} \left(\vec{\lambda}_i \right) \}$
            \EndWhile
        \end{algorithmic}
    \end{algorithm}

    \section{The empirical interpolation method}

    %FIXME
    The empirical interpolation method relies on finding the most relevant points
    in the physical space given the reduced basis.
    %
    The algorithm for finding the basis also follows a greedy approach, although
    the details are slightly different.
    %
    \begin{algorithm}
        \caption{EIM greedy algorithm}
        \label{algo:eim1}
        \begin{algorithmic}[1]
            \State Input: $\epsilon$, $\{\vec{\lambda}_{i}\}_{i=0}^{M}$

            \State Calculate: $\{\norm{\vec{h} \left( \vec{\lambda}_i \right)}\}_{i=0}^{M}$

            \State $i = 0$
            \State $\sigma_{i} = 1$
            \State $\vec{\lambda}_{i} = \norm{\vec{h} \left( \vec{\lambda}_i \right)}_{\argmax}$
            \State $RB = \{ \vec{h} \left(\vec{\lambda}_i \right) \}$

            \While{$\sigma_{i} > \epsilon$}
            \State $i = i+1$
            \State $\{c_{j,i-1} 
                = \braket{\vec{h} \left( \vec{\lambda}_j \right)}{\vec{e}_{i-1}} 
            \}_{j=0}^{M}$
            \State $\sigma_{i} = \left( \norm{\vec{h}}_j - \sum_k^{i-1}
                G^{-1}_{kl}c_{j,k} c_{j,l}\right)_{\max}$
            \State $\vec{\lambda}_{i} = \left( \norm{\vec{h}}_j - \sum_k^{i-1}
                G^{-1}_{kl}c_{j,k} c_{j,l}\right)_{\argmax}$
            \State $RB = RB \bigcup \{ \vec{h} \left(\vec{\lambda}_i \right) \}$
            \EndWhile
        \end{algorithmic}
    \end{algorithm}

    \section{Fast likelihood evaluations using the quadrature rule}

    This is a completely general method, described in the same paper by
    \textcite{Canizares2013}, but I will include the discussion of the posterior
    calculation here as well.

    We know, that our signal template can be expressed as a linear combination of
    the precomputed Reduced Basis functions:
    %
    \begin{equation}
        \vec{h} \left( x \right) \approx \sum_{i} a_{i}\vec{e}_{i} \left( x \right)
    \end{equation}
    %
    We also make it exactly match at the empirical interpolation points:
    %
    \begin{equation}
        \vec{h}_k = \sum_{i} a_{i} \vec{e}_{i} \left( F_{k} \right)
    \end{equation}
    %
    Hence, we can re-express the above equation as a system of equations in
    matrix notation (using summation convention):
    %
    \begin{equation}
        \vec{h}_{i} = A_{ij} a_{j} \implies a_{i} = A^{-1}_{ij} h_{j} 
    \end{equation} 
    %
    where $A_{ij} = \vec{e}_{i} \left( F_{j} \right) $.

    By expanding our residuals in a similar fashion, we can easily compute the
    inner product between the residuals and the signal template (using the
    summation convention):
    %
    \begin{align}
        \delta\vec{t} &\approx b_{i} \vec{e}_{i} 
        \implies
        b_{i} = \braket{\delta \vec{t}}{\vec{e}_{i}} 
        \\
        \braket{\vec{h}}{\delta\vec{t}} &\approx a_{i}
        \braket{\vec{e}_{i}}{\vec{e}_{j}} b_i
        = a_{i} G_{ij} b_{i}
        \\
        &= h_{i} A^{-1}_{ij} G_{jk} a_{k} \equiv h_{i} \widetilde{\delta t}_i
    \end{align}
    %
    Since the $\widetilde{\delta \vec{t}}$ can be precomputed before using the
    MCMC method, this greatly speeds up the posterior evaluations during the MCMC
    cycles.
    %
    The explicit form of $\delta \vec{t}$ is then:
    \begin{equation}
        \widetilde{\delta t}_i = A^{-1}_{ij} G_{jk}
        \braket{\delta \vec{t}}{\vec{e}_k}
    \end{equation}

    Also, we can use the same interpolation scheme in reducing the dimensionality
    of the $\braket{\vec{h}}{\vec{h}}$ inner product:
    %
    \begin{align}
        \braket{\vec{h}}{\vec{h}} &\approx a_{i} \braket{\vec{e}_{i}}{\vec{e}_{j}} a_i
        = a_{i} G_{ij} a_{i}
        \\
        &= h_{i} A^{-1}_{ij} G_{jk} A^{-1}_{kl} h_{l} 
        \equiv h_{i} B_{ij} h_{j}
    \end{align}
    %
    where the matrix $B$ can be also precomputed before the MCMC as it doesn't
    change given some basis.

    %
    Given all the derivations we have done, after constructing the ROQ rule, our
    evaluation of the logarithmic likelihood becomes:
    %
    \begin{equation}
        \loglike = \widetilde{\delta t}_{i} h_{i} -\cfrac{1}{2} h_{i} B_{ij} h_{j}
    \end{equation}
    %
    which means, that from two $\bigo{n^3}$ products we now have one $\bigo{m^2}$
    and one $\bigo{m^3}$ product, where $m$ is the number of the reduced basis
    used for calculations and sometimes we can have $m \ll n$, which means, that
    the calculations will be much faster.
    %
    Thus, the usage of the basis is very beneficial for computational speed-ups.

    \chapter{Parameter retrieval from modelled data}

    In this chapter I am going to discuss how well the parameters can be retrieved
    by using this compression method.
    %
    Also, I will discuss the errors when using this approach.

    \section{Exponential decrease of the approximation error when adding basis
        vectors into the reduced basis set}

    The paper by \textcite{Canizares2013} showed exponential error decrease in the
    approximation versus the number of basis vectors in the set.
    %
    This was also observed for different models and using different patches in the
    parameter space.
    %

    % TODO produce plots showing error decrease versus the number of basis in the
    % set. This would just be plotting a couple of file, but I need it to be
    % automated, so that when I add a new data file I can easily plot it.

    \section{Errors while approximating inner products}

    The method relies on the quadrature rule, which is used to calculate the inner
    products in the \loglike{} expression.
    %
    Here, I will be using different number of interpolation points and I will
    compare the full computation result to the approximation.

    % TODO produce some plots showing errors for a few selected waveforms. For
    % this I should use Cython, as it is quick enough. I need to be able to export
    % the matrices (or just the norms of the signal templates for this particular
    % task).
    %
    % The plots are: Percentage error in the estimation of the product vs number
    % of EIM points used (which corresponds to the number of reduced basis used)

    \section{Parameter retrieval using the Multinest software}

    As mentioned before parameter retrieval usually relies on MCMC techniques and
    as it is usually the case it is the best to use robust and well tested tools
    rather than trying to accomplish everything alone.
    %
    Exactly due to this reason I chose to use the Multinest sampler as it deals
    well with degeneracies, is efficient and easy to interface from the \cpp{}
    program I have written.
    %
    First we should test it whilst modelling noisy data containing a signal from
    only one source.

    % TODO code up the Multinest thing and output some data. This will probably be
    % yet another \cpp{} file to compile and run

    And then we can see how well this method is suited for examining noisy data
    containing multiple sources with different separations.

    \chapter{Discussion}

    \section{Significance of speed ups and single source search}

    Most people might say that speed-ups in this field might not be that relevant
    when we wait for data for 20 years and then we can analyse the data in a few
    days on a supercomputer.
    %
    % FIXME the argument is correct or not?
    However, this method might be invaluable in calibration of the data analysis
    tool-chain (is this correct?).
    %
    In this way checking for injected source signal data into experimental data
    and recovering the injected parameters might be very helpful to check, that
    there are no obvious mistakes in the procedure.

    %FIXME add more rambling about the importance of the computation speeds.

    \section{The validity of the method and concerns}

    \printbibliography

\end{document}

% vim: tw=82:colorcolumn=83:spell:spelllang=en_gb
