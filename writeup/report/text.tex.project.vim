" ATP project vim script: Tue Jul 30, 2013 at 02:43 pm +0100.

let b:atp_MainFile = 'text.tex'
let g:atp_mapNn = 0
let b:atp_autex = 1
let b:atp_TexCompiler = 'lualatex'
let b:atp_TexOptions = '-shell-escape,-synctex=1,-interaction=nonstopmode'
let b:atp_TexFlavor = 'tex'
let b:atp_auruns = '1'
let b:atp_ReloadOnError = '1'
let b:atp_OutDir = '/home/gns-ank/Documents/2012-2013 Summer Project/PTA/writeup/report'
let b:atp_OpenViewer = '1'
let b:atp_XpdfServer = 'text'
let b:atp_Viewer = 'okular'
let b:TreeOfFiles = {}
let b:ListOfFiles = []
let b:TypeDict = {}
let b:LevelDict = {}
let b:atp_BibCompiler = 'biber'
let b:atp_StarEnvDefault = ''
let b:atp_StarMathEnvDefault = ''
let b:atp_updatetime_insert = 4000
let b:atp_updatetime_normal = 2000
let b:atp_LocalCommands = ['\bigo{', '\arr{', '\loglike', '\argmax', '\cpp', '\bra{', '\ket{', '\braket{', '\matrixel{', '\norm{']
let b:atp_LocalEnvironments = []
let b:atp_LocalColors = []
