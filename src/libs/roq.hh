#include <vector>

/**
 * Generate the reduced basis set by using a greedy algorithm. This does not assume on
 * any functions whilst calculating the 
 *
 * @param N The number of points in the parameter space
 * @param getData A function of int, param_out and data_out variable, which would abstract the way
 *  I am getting data fram the parameter space.
 * @param A The matrix to use in the inner product
 * @param epsilon The maximum error for the reduced basis
 * @param RB_out The reduced basis output array
 * @param sigma_out The array for the error variation during the recursion
 * @param verbose should data be printed to stdout?
 */
void greedyReducedBasis (const unsigned long N,
                         void (*getData)(unsigned long idx, std::vector<double> & params_out, std::vector<double> & data_out),
                         std::vector<double> & A,
                         const double & epsilon,
                         std::vector<std::vector<double> > & RB_param_out,
                         std::vector<std::vector<double> > & RB_out,
                         std::vector<double> & G_out,
                         std::vector<double> & templateNorm_out,
                         std::vector<double> & sigma_out,
                         bool verbose = false);

/**
 * Generate a list of numbers from a single number. This acts as a good way to recover
 * data from a parameter space stored in an array sequentially
 *
 * @param idx A long number of choice, which should be in a range of the parameter
 *   space.
 * @param dim A list of dimensionalities of the parameter space.
 * @param list_out The output array
 */
void idToList (unsigned long idx, 
               std::vector<unsigned int> & dim, 
               std::vector<unsigned int> & list_out);

/**
 * Generate the reduced basis set by using a greedy algorithm. This does not assume on
 * any functions whilst calculating the 
 *
 * @param RB_param The reduced basis parameter array
 * @param RB The reduced basis array
 * @param indices_out The output array for the EIM indices
 * @param points_out The output array for the EIM points
 */
void greedyEIMpoints (std::vector<std::vector<double> > & RB,
                      std::vector<long> & idx_out,
                      std::vector<double> & points_out,
                      std::vector<double> & A_out);
/**
 * Reduce the order of the vector given the interpolation points
 *
 * @param idx The EIM indices
 * @param h The input vector
 * @param h_out The output vector
 */
void reduceVectorToEIMSample (std::vector<long> & idx,
                              std::vector<double> & h,
                              std::vector<double> & h_out);

/**
 * Generate a quadrature rule vector
 *
 * @param r The input vector, which will be modified
 * @param r_out The output vector
 * @param RB_hat The reduced bases premultiplied by a covariance matrix
 * @param G The grammian matrix
 * @param A The interpolation matrix
 */
void constructROQ (std::vector<double> & r,
                   std::vector<double> & r_out,
                   std::vector<std::vector<double> > & RB_hat,
                   std::vector<double> & G,
                   std::vector<double> & A);

/**
 * Construct the Interpolant using the EIM interpolation method
 *
 * @param RB The reduced basis set
 * @param h The signal template to interpolate
 * @param idx The EIM_indices
 * @param A_out The Interpolation matrix
 * @param data_out The interpolant
 */
void constructInterpolant (std::vector<std::vector<double> > & RB,
                           std::vector<double> & h,
                           std::vector<long> & idx,
                           std::vector<double> & A_out,
                           std::vector<double> & data_out);

/**
 * Construct the interpolation matrix
 *
 * @param RB The reduced basis set
 * @param idx The EIM_indices
 * @param N Number of reduced basis to use
 * @param A_out The Interpolation matrix
 */
void constructInterpolationMatrix (std::vector<std::vector<double> > & RB,
                                   std::vector<long> & idx,
                                   unsigned int N,
                                   std::vector<double> & A_out);

/**
 * Generate a quadrature rule covariance matrix
 *
 * @param C_out The output matrix
 * @param idx The indices vector
 * @param G The grammian matrix
 * @param A The interpolation matrix
 */
void constructROQCovMatrix (std::vector<double> & C_out,
                            std::vector<long> & idx,
                            std::vector<double> & G,
                            std::vector<double> & A);
