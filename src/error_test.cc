#include <iterator>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include <locale>
#include <ctime>

#include <vector>

#include "libs/pulsar.hh"
#include "libs/linalg.hh"
#include "libs/signal/model.hh"
#include "libs/signal/sampling.hh"
#include "libs/iocsv.hh"
#include "libs/roq.hh"

int main(int argc, char * argv []) {
    std::vector<std::string> fnames, argvs;
    std::string delim;
    for (int i = 0; i < argc ; i++) {
        argvs.push_back(argv[i]);
    }

    if (argc != 9) {
        std::cerr << "Not enough parameters!!! The usage is as follows:\n"
                  << "\t " << argvs[0] << " pulsars schedule sources test_sources rb_params eim_indices rh_output hh_output\n\n"
                  << "Meanings of the options are:\n"
                  << "\t TO BE DONE\n"
                  << std::endl;

        return 1;
    } else {
        std::cout << "\nCalculating the errors of approximation" << std::endl;
    }

    delim = ", ";

    std::vector<Pulsar> pulsars;
    std::vector<short unsigned> idx_all, idx_new;
    std::vector<long> EIM_i, EIM_i_test;
    std::vector<double> Times_all, Times_new, EIM_p, h, h_tmp, r_tilda, data, sigma_rh, sigma_hh;
    std::vector<std::vector<double> > RB_p_all, RB_test, RB_test_hat, sources, sources_test, params_test;
    std::vector<double> CovM, CovM_tilda, G, G_inv, A_inv;
    double rh_prec = 0, hh_prec = 0, rh_roq, hh_roq;

    // Read pulsar and schedule data from a file
    csv2arrayArrayDouble(argvs[1], sources, delim, true);
    csv2arrayArrayDouble(argvs[2], sources_test, delim, true);
    csv2pulsar(argvs[3], pulsars, delim, true);
    csv2arraysShortDouble(argvs[4], idx_all, Times_all, delim, true);
    csv2arrayArrayDouble(argvs[5], RB_p_all, delim, true);
    csv2arraysLongDouble(argvs[6], EIM_i, EIM_p, delim, true);

    // Debugging stuff. Check with a basis vector
    sources.clear(); sources.push_back(RB_p_all.at(5));
    sources_test = sources;

    try {
        genCovarianceMatrix (CovM, pulsars, idx_all, Times_all, true, false, false, false);

        generateSample (data, pulsars, idx_all, Times_all, sources);
        generateSample (h, pulsars, idx_all, Times_all, sources_test, false);

        rh_prec = innerProduct(data,CovM,h);
        hh_prec = innerProduct(h,CovM,h);

        std::cout << "Calculated precise values of the overlaps " << rh_prec << "\t" << hh_prec << std::endl;
    } catch (const char * msg) {
        std::cerr << msg << std::endl;
    }

    for (unsigned i = 0; i < RB_p_all.size() - 10; i++) {
        try {
            // include a new vector in the reduced basis set
            params_test.clear(); params_test.push_back(RB_p_all.at(i));
            generateSample (h_tmp, pulsars, idx_all, Times_all, params_test, false);
            RB_test.push_back(h_tmp);
            RB_test_hat.push_back(h_tmp);
            matrixVectorProduct (CovM, h_tmp, RB_test_hat.back());

            // Generate a new schedule with the eim points
            EIM_i_test.push_back(EIM_i.at(i));
            idx_new.push_back(idx_all.at(EIM_i_test.back()));
            Times_new.push_back(Times_all.at(EIM_i_test.back()));

            // Construct Grammian
            extendGrammianOptimized (G, RB_test, RB_test_hat);

            // Construct the interpolation matrix
            constructInterpolationMatrix(RB_test, EIM_i_test, RB_test.size(), A_inv);

            // Construct the ROQ Covariance Matrix for the <h|h> product speed up.
            constructROQCovMatrix(CovM_tilda, EIM_i_test, G, A_inv);

            // Construct the Roq for the <r|h> product
            r_tilda.clear();
            constructROQ (data, r_tilda, RB_test_hat, G, A_inv);

            reduceVectorToEIMSample(EIM_i_test, h, h_tmp);

            // evaluate products differently
            rh_roq = dotProduct(r_tilda, h_tmp);
            hh_roq = innerProduct(h_tmp, CovM_tilda, h_tmp);

            //std::cout << rh_roq << " " << hh_roq << std::endl;

            // push out the error
            sigma_rh.push_back(rh_roq/rh_prec);
            sigma_hh.push_back(hh_roq/hh_prec);

        } catch (const char * msg) {
            std::cerr << msg << std::endl;
        }
    }

    // Output the errors
    arrayDouble2csv (argvs[7], sigma_rh, delim, true);
    arrayDouble2csv (argvs[8], sigma_hh, delim, true);

    return 0;
}
