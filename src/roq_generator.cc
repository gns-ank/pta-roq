#include <iterator>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include <locale>
#include <ctime>

#include <vector>

#include "libs/pulsar.hh"
#include "libs/linalg.hh"
#include "libs/signal/model.hh"
#include "libs/signal/sampling.hh"
#include "libs/iocsv.hh"
#include "libs/roq.hh"

int main(int argc, char * argv []) {
    std::vector<std::string> fnames, argvs;
    std::string delim;
    for (int i = 0; i < argc ; i++) {
        argvs.push_back(argv[i]);
    }

    if (argc != 6) {
        std::cerr << "Not enough parameters!!! The usage is as follows:\n"
                  << "\t " << argvs[0] << " pulsars schedule sources rb_params eim_output roq_output\n\n"
                  << "Meanings of the options are:\n"
                  << "\t rc The configuration file\n"
                  << "\t rc_source The parameters for the source\n"
                  << "\t date Time stamp in whatever format you want, but it should be preferably YYYY-MM-DD-HH-MM-SS\n"
                  << "\t date_in Time stamp in whatever format you want. This should denote the time stamp for the file you want to read\n"
                  << "\t error An error to achieve in the RB approximation\n"
                  << std::endl;

        return 1;
    } else {
        std::cout << "\nCalculating the errors of approximation" << std::endl;
    }

    if (parseRoqRC (argvs[1], argvs[3], argvs[4], fnames, delim) == 1) {
        std::cerr <<        "The syntax of the config file was incorrect. The syntax is described bellow: "
            << std::endl << "The key (i.e. the first value) matters here! But the whitespace doesn't unless it is a"
            << std::endl << "commented line. Also, the order in which the options are given doesn't matter either."
            << std::endl << "Commented lines start either with #."
            << std::endl << ""
            << std::endl << "Available keys:"
            << std::endl << "\t- ext The extension of the files"
            << std::endl << "\t- delim The delimiter used"
            << std::endl << "\t- sep The separator used in the filename"
            << std::endl << ""
            << std::endl << "\t- dir.in The output directory"
            << std::endl << "\t- prefix.in The prefix for the data input files"
            << std::endl << "\t- infile.pulsar Input pulsar parameters"
            << std::endl << "\t- infile.sched Input time stamps and indices of the timed pulsars"
            << std::endl << "\t- infile.resid Input the generated residuals"
            << std::endl << "\t- infile.rbparams Input the reduced basis"
            << std::endl << ""
            << std::endl << "\t- dir.out The output directory"
            << std::endl << "\t- prefix.out The prefix for the data output files"
            << std::endl << "\t- outfile.eim Ouput the reduced basis"
            << std::endl << "\t- outfile.sched Ouput the parameters used to construct the reduced basis"
            << std::endl << "\t- outfile.resid Ouput the parameters used to construct the reduced basis"
            << std::endl << "\t- outfile.covm Ouput the parameters used to construct the reduced basis"
            << std::endl;
    }

    delim = ", ";

    std::vector<Pulsar> pulsars;
    std::vector<short unsigned> idx_all, idx_new;

    std::vector<long> EIM_i;

    std::vector<double> Times_all, Times_new, EIM_p, h, h_tmp, r_tilda, data, sigma_rh, sigma_hh;
    std::vector<std::vector<double> > RB_p_all, RB_all, RB_all_hat, sources; 
    std::vector<double> CovM, CovM_tilda, G, G_inv, A_inv;

    // Read data generated in the previous steps
    csv2pulsar(fnames[0], pulsars, delim, true);
    csv2arraysShortDouble(fnames[1], idx_all, Times_all, delim, true);
    csv2arrayDouble(fnames[2], data, delim, true);
    csv2arrayArrayDouble(fnames[3], RB_p_all, delim, true);
    csv2arrayArrayDouble(argvs[2], sources, delim, true);

    try {
        std::clock_t start = std::clock();

        // Generate the covariance matrix
        genCovarianceMatrix (CovM, pulsars, idx_all, Times_all, true, false, false, false);
        std::cout << "The job was done in " << ( std::clock() - start ) / (double) CLOCKS_PER_SEC << " s of CPU Time" << std::endl;

        // Generate the reduced basis from their parameters and construct the grammian
        // matrix, which will be needed later
        for (unsigned i = 0; i < RB_p_all.size(); i++) {
            std::vector<std::vector<double>> rb_source;
            rb_source.clear(); rb_source.push_back(RB_p_all.at(i));
            generateSample (data, pulsars, idx_all, Times_all, rb_source, false);
            RB_all.push_back(data);
            RB_all_hat.push_back(data);
            matrixVectorProduct (CovM, RB_all.back(), RB_all_hat.back());

            // Construct Grammian
            extendGrammianOptimized (G, RB_all, RB_all_hat);
        }
        std::cout << "The job was done in " << ( std::clock() - start ) / (double) CLOCKS_PER_SEC << " s of CPU Time" << std::endl;
        greedyEIMpoints (RB_all, EIM_i, EIM_p, A_inv);
        std::cout << "The job was done in " << ( std::clock() - start ) / (double) CLOCKS_PER_SEC << " s of CPU Time" << std::endl;
        constructROQ (data, r_tilda, RB_all_hat, G, A_inv);
        std::cout << "The job was done in " << ( std::clock() - start ) / (double) CLOCKS_PER_SEC << " s of CPU Time" << std::endl;
        constructROQCovMatrix(CovM_tilda, EIM_i, G, A_inv);
        std::cout << "The job was done in " << ( std::clock() - start ) / (double) CLOCKS_PER_SEC << " s of CPU Time" << std::endl;

        // Generate a new timetable
        for (unsigned i = 0; i < RB_p_all.size(); i++) {
            idx_new.push_back(idx_all.at(EIM_i.back()));
            Times_new.push_back(Times_all.at(EIM_i.back()));
        }

        // Output everything
        arraysLongDouble2csv(fnames[4], EIM_i, EIM_p, delim, true);
        arraysShortDouble2csv(fnames[5], idx_new, Times_new, delim, true);
        arrayDouble2csv(fnames[6], r_tilda, delim, true);
        arrayDouble2csv(fnames[7], CovM_tilda, delim, true);
    } catch (const char * msg) {
        std::cerr << msg << std::endl;
    }

    return 0;
}
